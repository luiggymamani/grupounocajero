import Model.SpecialCondition;
import Model.StandardUser;
import Model.User;
import Model.UserQueue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUserQueue {

    @Test
    public void testAddForNormalUsers() {
        UserQueue userQueue = new UserQueue();

        userQueue.add(new StandardUser("Joel", 18, true, 100, 1001));
        userQueue.add(new StandardUser("Jess", 22, false, 150, 1002));
        userQueue.add(new StandardUser("Pedro", 25, true, 200, 1003));

        int sizeExpected = 3;
        assertEquals(sizeExpected, userQueue.size());

        // checking the order in which they were added

        String nameUserExpected = "Joel";
        int idUserExpected = 1001;
        User userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());

        nameUserExpected = "Jess";
        idUserExpected = 1002;
        userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());

        nameUserExpected = "Pedro";
        idUserExpected = 1003;
        userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());
    }

    @Test
    public void testAddSortForSpecialUsers() {
        UserQueue userQueue = new UserQueue();

        // user special
        User userSpecial1 = new StandardUser("Helen", 19, false, 300, 1004);
        userSpecial1.setIspecial(true, SpecialCondition.SENIOR_CITIZEN);
        User userSpecial2 = new StandardUser("Mike", 25, true, 300, 1005);
        userSpecial2.setIspecial(true, SpecialCondition.HANDICAPPED_PERSON);

        userQueue.add(new StandardUser("Joel", 18, true, 100, 1001));
        userQueue.add(new StandardUser("Jess", 22, false, 150, 1002));
        userQueue.add(userSpecial1);
        userQueue.add(new StandardUser("Pedro", 25, true, 200, 1003));
        userQueue.add(userSpecial2);

        // checking the order in which they were added
        String nameUserExpected = "Helen";
        int idUserExpected = 1004;
        User userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());

        nameUserExpected = "Mike";
        idUserExpected = 1005;
        userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());

        nameUserExpected = "Joel";
        idUserExpected = 1001;
        userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());

        nameUserExpected = "Jess";
        idUserExpected = 1002;
        userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());

        nameUserExpected = "Pedro";
        idUserExpected = 1003;
        userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());
    }

    @Test
    public void testSortUsers() {
        // users are ordered when some become special

        UserQueue userQueue = new UserQueue();
        User user1 = new StandardUser("Helen", 19, false, 300, 1004);
        User user2 = new StandardUser("Mike", 25, true, 300, 1005);
        User user3 = new StandardUser("Pedro", 19, true, 300, 1006);
        User user4 = new StandardUser("Jess", 25, false, 300, 1007);
        userQueue.add(user1);
        userQueue.add(user2);
        userQueue.add(user3);
        userQueue.add(user4);

        // the queue is INVERTED when printing
        String expectedQueue = "Jess, Pedro, Mike, Helen";
        assertEquals(expectedQueue, userQueue.showQueue());

        user3.setIspecial(true, SpecialCondition.BYTIMEONHOLD);
        expectedQueue = "Jess, Mike, Helen, Pedro";
        userQueue.sort();
        assertEquals(expectedQueue, userQueue.showQueue());

        user4.setIspecial(true, SpecialCondition.BYTIMEONHOLD);
        expectedQueue = "Mike, Helen, Jess, Pedro";
        userQueue.sort();
        assertEquals(expectedQueue, userQueue.showQueue());

        user2.setIspecial(true, SpecialCondition.BYTIMEONHOLD);
        expectedQueue = "Helen, Mike, Jess, Pedro";
        userQueue.sort();
        assertEquals(expectedQueue, userQueue.showQueue());

        user2.setIspecial(true, SpecialCondition.BYTIMEONHOLD);
        expectedQueue = "Helen, Mike, Jess, Pedro";
        userQueue.sort();
        assertEquals(expectedQueue, userQueue.showQueue());

        // removing users in order

        String nameUserExpected = "Pedro";
        int idUserExpected = 1006;
        User userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());

        nameUserExpected = "Jess";
        idUserExpected = 1007;
        userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());

        nameUserExpected = "Mike";
        idUserExpected = 1005;
        userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());

        nameUserExpected = "Helen";
        idUserExpected = 1004;
        userExpected = userQueue.deque();
        assertEquals(nameUserExpected, userExpected.getNameUser());
        assertEquals(idUserExpected, userExpected.getId());
    }
}
