import Model.*;

import org.junit.jupiter.api.Test;
public class TestMessage {
    Message m = new Message();

    @Test
    public void message() throws InterruptedException {
        m.finishedUser("Paco");

        m.generalReport(12, 13, 24, 5, 20);
    }

    @Test
    public void tail()throws InterruptedException {
        UserQueue queue = new UserQueue();
        for (int i = 0; i < 30; i++) {
            String name = "Paco" + (i + 1);
            queue.add(new StandardUser(name, 30, true, 1000, i));
        }

        m.tail(queue);
    }
}
