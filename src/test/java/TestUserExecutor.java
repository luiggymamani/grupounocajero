import Model.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUserExecutor {
    @Test
    public void executeUserTest() throws InterruptedException {
        Bank bank = new Bank("Bisa", "Av America", 1000);
        WaterService wt = new WaterService("Emapa", "Av Blanco Galindo", 100);
        LightService lt = new LightService("Elfec", "Av ramon rivero", 100);
        InternetService it = new InternetService("Tigo", "Av Pando", 100);
        TellerMachine tellerMachine = new TellerMachine(bank, wt, lt, it);

        UserQueue queue = new UserQueue();

        UserExecutor us = new UserExecutor(tellerMachine, queue);
        UserCreator uc = new UserCreator();

        ArrayList<User> userArrayList1 = uc.generateStandardUser(5);
        ArrayList<User> userArrayList2 = uc.generateSpecialStandardUser(5);
        ArrayList<User> userArrayList3 = uc.generateClient(5);

        us.addUsersToQueue(userArrayList1);
        us.addUsersToQueue(userArrayList2);
        us.addUsersToQueue(userArrayList3);

        assertEquals(3000005, bank.getRegisteredUsers().size());

        us.executeUser();
        us.executeUser();
        us.executeUser();

        assertEquals(12, queue.size());


        assertEquals("userSpecial 3000007", us.getCurrentUser().getNameUser());
        assertEquals(900, us.getCurrentUser().getUserMoney());
        assertEquals(true, us.getCurrentUser().isSpecial());
    }
}
