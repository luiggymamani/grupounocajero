import Model.SpecialCondition;
import Model.User;
import Model.UserCreator;
import Model.UserQueue;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUserCreator {
    @Test
    public void createStandardUsersTest(){
        UserCreator us = new UserCreator();


        ArrayList<User> userArrayList = us.generateStandardUser(3);

        assertEquals(3, userArrayList.size());

        assertEquals(false, userArrayList.isEmpty());

        assertEquals("userStandard 3", userArrayList.get(0).getNameUser());
        assertEquals("userStandard 4", userArrayList.get(1).getNameUser());
        assertEquals("userStandard 5", userArrayList.get(2).getNameUser());
    }

    @Test
    public void createSpecialUsersTest(){
        UserCreator us = new UserCreator();

        ArrayList<User> userArrayList = us.generateSpecialStandardUser(3);

        assertEquals(3, userArrayList.size());
        assertEquals(false, userArrayList.isEmpty());
        assertEquals(true, userArrayList.get(0).isSpecial());
        assertEquals(true, userArrayList.get(1).isSpecial());
        assertEquals(true, userArrayList.get(2).isSpecial());
    }

    @Test
    public void createClientsTest(){
        UserCreator us = new UserCreator();

        ArrayList<User> userArrayList = us.generateClient(3);
        assertEquals(3, userArrayList.size());
        assertEquals(false, userArrayList.isEmpty());
        assertEquals(true, userArrayList.get(0).isSpecial());
        assertEquals(true, userArrayList.get(1).isSpecial());
        assertEquals(true, userArrayList.get(2).isSpecial());

        assertEquals(SpecialCondition.CLIENT, userArrayList.get(0).getSpecialCondition());
        assertEquals(SpecialCondition.CLIENT, userArrayList.get(1).getSpecialCondition());
        assertEquals(SpecialCondition.CLIENT, userArrayList.get(2).getSpecialCondition());
    }
}
