import Model.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class TestPurpose {

    Bank bank = new Bank("Mercantil Santa Cruz", "Cochabamba", 5000);
    LightService lightService = new LightService("ENDE", "Cochabamba", 100);
    InternetService internetService = new InternetService("Comteco", "Cochabamba", 200);
    WaterService waterService = new WaterService("Misicuni", "Cochabamba", 70);
    TellerMachine tellerMachine = new TellerMachine(bank, waterService, lightService, internetService);

    @Test
    public void purpose() throws InterruptedException {
        Client client = new Client("Paco",30,true,1000,1);
        tellerMachine.getBank().registerUser(client);

        assertEquals(1000,client.getUserMoney());
        client.purpose(tellerMachine);
        assertNotEquals(1000,client.getUserMoney());
        assertNotNull(client.getReasonForUsing());

        Client client1 = new Client("Pepe",60,true,0,2);
        tellerMachine.getBank().registerUser(client1);

        client1.purpose(tellerMachine);
        if (client1.getReasonForUsing().equals("Deposit Money")){
            //si deposito dinero deberia ser mas que cero
            assertNotEquals(0,client1.getUserMoney());
        }

        StandardUser user = new StandardUser("Pepa", 40,false,1000,3);
        tellerMachine.getBank().registerUser(user);

        assertEquals(1000,user.getUserMoney());
        user.purpose(tellerMachine);
        assertNotEquals(1000,user.getUserMoney());
        assertNotNull(client.getReasonForUsing());

        StandardUser user1 = new StandardUser("Mari",35,false,0,4);
        tellerMachine.getBank().registerUser(user1);
        user1.purpose(tellerMachine);
        // el user no tiene dinero para pagar asi que no deberia de pasar nada
        assertEquals(0,user1.getUserMoney());
        assertEquals("",user1.getReasonForUsing());
    }
}

