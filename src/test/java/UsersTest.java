import Model.SpecialCondition;
import Model.StandardUser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UsersTest {
    @Test
    public void userTest(){
        StandardUser user1 = new StandardUser("pepe", 25, true, 1000,1234);
        user1.setIspecial(true, SpecialCondition.SENIOR_CITIZEN);

        String nameUserExpected = "pepe";
        assertEquals(nameUserExpected, user1.getNameUser());

        StandardUser user2 = new StandardUser("maria", 35, false, 1000, 5678);
        user2.setIspecial(false, null);

        nameUserExpected = "maria";
        assertEquals(nameUserExpected, user2.getNameUser());
    }
}
