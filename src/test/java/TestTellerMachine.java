import Model.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestTellerMachine {

    Bank bank = new Bank("Mercantil Santa Cruz", "Cochabamba", 5000);
    LightService lightService = new LightService("ENDE", "Cochabamba", 100);
    InternetService internetService = new InternetService("Comteco", "Cochabamba", 200);
    WaterService waterService = new WaterService("Misicuni", "Cochabamba", 70);
    TellerMachine tellerMachine = new TellerMachine(bank, waterService, lightService, internetService);


    @Test
    public void testRegisteredUserToBank() {
        bank.registerUser(new Client("Pedro", 19, true, 600, 101));
        bank.registerUser(new Client("Jess", 20, false, 300, 102));
        bank.registerUser(new Client("Juan", 25, true, 50, 103));
        bank.registerUser(new Client("Oriana", 25, false, 200, 104));
        bank.registerUser(new Client("Cristian", 19, true, 900, 105));
        bank.registerUser(new Client("Helen", 18, false, 700, 106));

        int sizeExpected = 6;
        assertEquals(sizeExpected, bank.getRegisteredUsers().size());

        String userNameExpected = "Pedro";
        int userIdExpected = 101;
        assertEquals(userNameExpected, bank.getRegisteredUsers().get(101).getNameUser());
        assertEquals(userIdExpected, bank.getRegisteredUsers().get(101).getId());

        userNameExpected = "Jess";
        userIdExpected = 102;
        assertEquals(userNameExpected, bank.getRegisteredUsers().get(102).getNameUser());
        assertEquals(userIdExpected, bank.getRegisteredUsers().get(102).getId());

        userNameExpected = "Juan";
        userIdExpected = 103;
        assertEquals(userNameExpected, bank.getRegisteredUsers().get(103).getNameUser());
        assertEquals(userIdExpected, bank.getRegisteredUsers().get(103).getId());

        userNameExpected = "Oriana";
        userIdExpected = 104;
        assertEquals(userNameExpected, bank.getRegisteredUsers().get(104).getNameUser());
        assertEquals(userIdExpected, bank.getRegisteredUsers().get(104).getId());

        userNameExpected = "Cristian";
        userIdExpected = 105;
        assertEquals(userNameExpected, bank.getRegisteredUsers().get(105).getNameUser());
        assertEquals(userIdExpected, bank.getRegisteredUsers().get(105).getId());

        userNameExpected = "Helen";
        userIdExpected = 106;
        assertEquals(userNameExpected, bank.getRegisteredUsers().get(106).getNameUser());
        assertEquals(userIdExpected, bank.getRegisteredUsers().get(106).getId());

        // register users with the already registered ID
        tellerMachine.getBank().registerUser(new Client("Jerai", 19, true, 600, 101));
        tellerMachine.getBank().registerUser(new Client("Axel", 20, true, 300, 102));

        assertEquals(sizeExpected, bank.getRegisteredUsers().size());
    }

    @Test
    public void testActionsWithTheBank() throws InterruptedException {
        testRegisteredUserToBank();
        // Deposit money
        User user1 = bank.getRegisteredUsers().get(101);
        tellerMachine.getBank().depositMoney(user1, 200);
        int userMoneyExpected = 400;
        int bankMoneyExpected = 5200;
        assertEquals(userMoneyExpected, user1.getUserMoney());
        assertEquals(bankMoneyExpected, bank.getMoneyDeposited());

        // Withdraw Money
        User user2 = bank.getRegisteredUsers().get(102);
        tellerMachine.getBank().withdrawMoney(user2, 500);
        userMoneyExpected = 800;
        bankMoneyExpected = 4700;
        assertEquals(userMoneyExpected, user2.getUserMoney());
        assertEquals(bankMoneyExpected, bank.getMoneyDeposited());

        // Deposit money that the user does not have
        User user3 = bank.getRegisteredUsers().get(103);
        tellerMachine.getBank().depositMoney(user3, 1000);
        userMoneyExpected = 50;
        assertEquals(userMoneyExpected, user3.getUserMoney());
        assertEquals(bankMoneyExpected, bank.getMoneyDeposited());

        // Withdraw money that the bank does not have
        User user4 = bank.getRegisteredUsers().get(104);
        tellerMachine.getBank().withdrawMoney(user4, 12000);
        userMoneyExpected = 200;
        assertEquals(userMoneyExpected, user4.getUserMoney());
        assertEquals(bankMoneyExpected, bank.getMoneyDeposited());
    }

    @Test
    public void testActionsWithWaterService() throws InterruptedException {
        WaterService waterService = tellerMachine.getWaterService();

        User user1 = new StandardUser("Axel", 19, true, 300, 1002);
        int userMoneyExpected = user1.getUserMoney() - waterService.getAmountToPay();
        int serviceMoneyExpected = waterService.getMoneyCollected() + waterService.getAmountToPay();
        waterService.payForTheService(user1);
        assertEquals(userMoneyExpected, user1.getUserMoney());
        assertEquals(serviceMoneyExpected, waterService.getMoneyCollected());

        // User who did not have enough money to pay
        User user2 = new StandardUser("Nelson", 35, true, 0, 1003);
        userMoneyExpected = 0;
        serviceMoneyExpected = waterService.getMoneyCollected();
        waterService.payForTheService(user2);
        assertEquals(userMoneyExpected, user2.getUserMoney());
        assertEquals(serviceMoneyExpected, waterService.getMoneyCollected());
    }

    @Test
    public void testActionsWithInternetService() throws InterruptedException {
        InternetService internetService = tellerMachine.getInternetService();

        User user1 = new StandardUser("Camila", 19, false, 400, 1002);
        int userMoneyExpected = user1.getUserMoney() - internetService.getAmountToPay();
        int serviceMoneyExpected = internetService.getMoneyCollected() + internetService.getAmountToPay();
        internetService.payForTheService(user1);
        assertEquals(userMoneyExpected, user1.getUserMoney());
        assertEquals(serviceMoneyExpected, internetService.getMoneyCollected());

        // User who did not have enough money to pay
        User user2 = new StandardUser("Jose", 35, true, 0, 1003);
        userMoneyExpected = 0;
        serviceMoneyExpected = internetService.getMoneyCollected();
        internetService.payForTheService(user2);
        assertEquals(userMoneyExpected, user2.getUserMoney());
        assertEquals(serviceMoneyExpected, internetService.getMoneyCollected());
    }

    @Test
    public void testActionsWithLightService() throws InterruptedException {
        LightService lightService = tellerMachine.getLightService();

        User user1 = new StandardUser("Samuel", 19, true, 500, 1002);
        int userMoneyExpected = user1.getUserMoney() - lightService.getAmountToPay();
        int serviceMoneyExpected = lightService.getMoneyCollected() + lightService.getAmountToPay();
        lightService.payForTheService(user1);
        assertEquals(userMoneyExpected, user1.getUserMoney());
        assertEquals(serviceMoneyExpected, lightService.getMoneyCollected());

        // User who did not have enough money to pay
        User user2 = new StandardUser("John", 35, true, 0, 1003);
        userMoneyExpected = 0;
        serviceMoneyExpected = lightService.getMoneyCollected();
        lightService.payForTheService(user2);
        assertEquals(userMoneyExpected, user2.getUserMoney());
        assertEquals(serviceMoneyExpected, lightService.getMoneyCollected());
    }
}
