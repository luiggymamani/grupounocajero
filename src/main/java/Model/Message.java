package Model;

public class Message {
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    public static final String ANSI_RESET = "\u001B[0m";

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";

    public void userAttended(User user,int time) throws InterruptedException {
        String gender = "M";
        if (!user.getIsMale()) gender = "F";
        String ageS = "" + user.getAge();
        System.out.println(ANSI_YELLOW + ANSI_BLACK_BACKGROUND + "##### Current client attended #####" + ANSI_RESET);
        System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "  Name : %35s\n" ,ANSI_WHITE + user.getNameUser() + "  " + ANSI_RESET);
        System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "  Age: %10s",ANSI_WHITE + ageS);
        System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "        Gender: %12s",ANSI_WHITE + gender + "  ");
        System.out.print( ANSI_RESET + "\n");
        System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "  Reason:%35s\n",ANSI_WHITE + ""+user.getReasonForUsing() + "  " + ANSI_RESET);
        System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "  Is special :   %27s \n",ANSI_WHITE + "" + user.isSpecial() + "  "   + ANSI_RESET);
        if(user.getSpecialCondition() != null){
            System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "  Condition :%31s \n",ANSI_WHITE + "" + user.getSpecialCondition().name() + "  "   + ANSI_RESET);
        }
        System.out.println(ANSI_YELLOW + ANSI_BLACK_BACKGROUND + "###################################" + ANSI_RESET + "\n");
        Thread.sleep(time);
    }

    public void finishedUser(String name) throws InterruptedException {
        System.out.println(ANSI_RED + ANSI_BLACK_BACKGROUND + "###################################" + ANSI_RESET);
        System.out.printf(ANSI_RED + ANSI_BLACK_BACKGROUND + "  User : %30s\n" ,name + "  " + ANSI_RESET);
        System.out.println(ANSI_RED + ANSI_BLACK_BACKGROUND + "      Has finished his turn.       " + ANSI_RESET);
        System.out.println(ANSI_RED + ANSI_BLACK_BACKGROUND + "###################################" + ANSI_RESET + "\n");
        Thread.sleep(1000);
    }
    public void generalReport(int electricServ, int waterServ, int ethernetServ, int registeredUsers, int depositMoney) throws InterruptedException {
        System.out.println(ANSI_YELLOW + ANSI_BLACK_BACKGROUND + "######### General  Report #########" + ANSI_RESET);
        System.out.println(ANSI_YELLOW + ANSI_BLACK_BACKGROUND + "              SERVICES             " + ANSI_RESET);
        System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "  Electric service:%25s\n",ANSI_WHITE + electricServ + "  " + ANSI_RESET);
        System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "  Water service:   %25s\n",ANSI_WHITE + waterServ + "  " + ANSI_RESET);
        System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "  Ethernet service:%25s\n",ANSI_WHITE + ethernetServ + "  " + ANSI_RESET);
        System.out.println(ANSI_YELLOW + ANSI_BLACK_BACKGROUND + "                BANK               " + ANSI_RESET);
        System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "  Registered Users:%25s\n",ANSI_WHITE + registeredUsers + "  " + ANSI_RESET);
        System.out.printf(ANSI_CYAN + ANSI_BLACK_BACKGROUND + "  Deposit Money:   %25s\n",ANSI_WHITE + depositMoney + "  " + ANSI_RESET);
        System.out.println(ANSI_YELLOW + ANSI_BLACK_BACKGROUND + "###################################" + ANSI_RESET + "\n");
        Thread.sleep(1000);
    }

    public void tail(UserQueue queue) throws InterruptedException {
        System.out.println(ANSI_YELLOW + ANSI_BLACK_BACKGROUND + "############## TAIL ###############" + ANSI_RESET);
        String fila = "";
        int index = queue.size();
        System.out.printf(ANSI_GREEN + ANSI_BLACK_BACKGROUND + " (%-4d)  Users on standby          " + ANSI_RESET + "\n",index);
        System.out.printf(ANSI_GREEN + ANSI_BLACK_BACKGROUND +  " (%s)  color of special users    "+ ANSI_RESET + "\n",ANSI_CYAN + "CYAN" );
        System.out.printf(ANSI_GREEN + ANSI_BLACK_BACKGROUND +  " (%s )  color of non-special users"+ ANSI_RESET + "\n",ANSI_RED + "RED" );
        while (index > 0){
            fila = "";
            int limit = Math.min(index, 10);
            for (int i = 0; i < limit; i++) {
                if(queue.getUser(index-1).isSpecial()){
                    fila += ANSI_RED + "(" + queue.getUser(index-1).getWaitingShifts() + ")" + queue.getUser(index-1).getNameUser();
                }else {
                    fila += ANSI_CYAN + "(" + queue.getUser(index-1).getWaitingShifts() + ")" + queue.getUser(index-1).getNameUser();
                }

                if (i != (limit-1)){
                    fila += ANSI_WHITE + " -> ";
                }
                index --;
            }
            System.out.println(ANSI_BLACK_BACKGROUND + fila + ANSI_RESET);
        }
        System.out.println(ANSI_YELLOW + ANSI_BLACK_BACKGROUND + "###################################" + ANSI_RESET + "\n");
        Thread.sleep(1000);
    }

    public void actionUser(String name) throws InterruptedException {
        System.out.println(ANSI_GREEN + ANSI_BLACK_BACKGROUND + "###################################" + ANSI_RESET);
        System.out.printf(ANSI_GREEN + ANSI_BLACK_BACKGROUND + "  User : %30s\n" ,name + "  " + ANSI_RESET);
        System.out.println(ANSI_GREEN + ANSI_BLACK_BACKGROUND +  "       Performing an action        " + ANSI_RESET);
        System.out.println(ANSI_GREEN + ANSI_BLACK_BACKGROUND + "###################################" + ANSI_RESET + "\n");
        Thread.sleep(1000);
    }

    public void showOptions() {
        System.out.println(" ".repeat(30) + ANSI_PURPLE + "Options");
        System.out.println(" ".repeat(3) + ANSI_CYAN + "1. Standard User" + " ".repeat(3) + "2. Special User" +
                " ".repeat(3) + "3. Client");
    }

    public void askForTypeOfUser() {
        System.out.print(ANSI_YELLOW + "Type of user   -->   ");
    }

    public void askForAmountByUser() {
        System.out.print(ANSI_PURPLE + "Users quantity    -->   ");
    }
}
