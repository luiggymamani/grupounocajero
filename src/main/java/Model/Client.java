package Model;

public class Client extends User{

    public Client(String name, int age, boolean isMale, int UserMoney, int id) {
        super(name, age, isMale, UserMoney, id);
        this.setTime(3000);
    }

    @Override
    public void purpose(TellerMachine tellerMachine) throws InterruptedException {
        int random = (int) Math.floor(Math.random()*(2-1+1)+1);
        int money = (int) Math.floor(Math.random()*(500-100+1)+100);

        switch (random){
            case 1:
                tellerMachine.bank.depositMoney(this,money);
                break;
            case 2:
                tellerMachine.bank.withdrawMoney(this,money);
        }
    }
}
