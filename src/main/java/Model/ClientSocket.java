package Model;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class ClientSocket {

    UserCreator userCreator;
    Socket socket;
    String host;
    int port;
    ObjectOutputStream outputStream;
    ArrayList<User> users;
    Scanner scanner;
    Message messages;

    public ClientSocket(String host, int port) {
        this.port = port;
        this.host = host;
        scanner = new Scanner(System.in);
        messages = new Message();
        userCreator = new UserCreator();
    }

    public void aksForTask() {
        messages.showOptions();
        int amountUser;
        int userTypeOption;
        while (true) {
            startAll();
            messages.askForTypeOfUser();
            userTypeOption = scanner.nextInt();
            messages.askForAmountByUser();
            amountUser = scanner.nextInt();
            generateUsers(userTypeOption, amountUser);
            sendTask();
        }
    }

    public void sendTask() {
        try {
            outputStream.writeObject(users);
            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void startAll() {
        users = new ArrayList<>();
        try {
            socket = new Socket(host, port);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void generateUsers(int userTypeOption, int amountUsers) {
        switch (userTypeOption) {
            case 1:
                users = userCreator.generateStandardUser(amountUsers);
                break;
            case 2:
                users = userCreator.generateSpecialStandardUser(amountUsers);
                break;
            case 3:
                users = userCreator.generateClient(amountUsers);
                break;
            default:
                System.out.println("\tOption no found");
        }
    }
}
