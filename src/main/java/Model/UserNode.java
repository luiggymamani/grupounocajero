package Model;

class UserNode {

    User user;
    UserNode nextUser;

    public UserNode(User userData) {
        this.user = userData;
        nextUser = null;
    }

    public User getUser() {
        return user;
    }

    public UserNode getNextUser() {
        return nextUser;
    }

    public void setNextUser(UserNode nextUser) {
        this.nextUser = nextUser;
    }
}
