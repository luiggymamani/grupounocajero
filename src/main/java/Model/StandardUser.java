package Model;

public class StandardUser extends User{


    public StandardUser(String name, int age, boolean isMale, int UserMoney, int id) {
        super(name, age, isMale, UserMoney, id);
        this.setTime(1500);
    }

    @Override
    public void purpose(TellerMachine tellerMachine) throws InterruptedException {
        int random = (int) Math.floor(Math.random()*(3-1+1)+1);
        switch (random){
            case 1:
                tellerMachine.lightService.payForTheService(this);
                break;
            case 2:
                tellerMachine.waterService.payForTheService(this);
                break;
            case 3:
                tellerMachine.internetService.payForTheService(this);
                break;
        }
    }
}
