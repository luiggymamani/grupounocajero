package Model;

import java.util.ArrayList;

public class TellerMachine {

    Bank bank;
    WaterService waterService;
    LightService lightService;
    InternetService internetService;
    ArrayList<User> usersWhoUsedTheATM;

    public TellerMachine(Bank bank, WaterService waterService, LightService lightService, InternetService internetService) {
        this.bank = bank;
        this.waterService = waterService;
        this.lightService = lightService;
        this.internetService = internetService;
        usersWhoUsedTheATM = new ArrayList<>();
    }

    public void addUserToATM(User user) {
        usersWhoUsedTheATM.add(user);
    }

    public Bank getBank() {
        return bank;
    }

    public WaterService getWaterService() {
        return waterService;
    }

    public LightService getLightService() {
        return lightService;
    }

    public InternetService getInternetService() {
        return internetService;
    }

    public ArrayList<User> getUsersWhoUsedTheATM() {
        return usersWhoUsedTheATM;
    }
}
