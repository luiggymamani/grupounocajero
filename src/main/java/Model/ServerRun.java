package Model;

public class ServerRun {

    public static void main(String[] args) {

        final int serverPort = 5000;
        UserQueue userQueue = new UserQueue();

        Bank bank = new Bank("Mercantil Santa Cruz", "Cochabamba", 5000);
        LightService lightService = new LightService("ENDE", "Cochabamba", 100);
        InternetService internetService = new InternetService("Comteco", "Cochabamba", 200);
        WaterService waterService = new WaterService("Misicuni", "Cochabamba", 70);
        TellerMachine tellerMachine = new TellerMachine(bank, waterService, lightService, internetService);

        UserExecutor userExecutor = new UserExecutor(tellerMachine, userQueue);
        ServerSocket serverSocket = new ServerSocket(serverPort, userQueue);

        Thread threadAdd = new Thread(serverSocket);
        Thread threadExecution = new Thread(userExecutor);

        threadAdd.start();
        threadExecution.start();
    }
}
