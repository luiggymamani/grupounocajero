package Model;

public class WaterService extends Service{

    public WaterService(String nameService, String locationService, int amountToPay) {
        super(nameService, locationService, amountToPay,"Pay Water Service");
    }

}
