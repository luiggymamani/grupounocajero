package Model;

import java.util.ArrayList;

public class Service {

    private String nameService;
    private String locationService;
    private int moneyCollected;
    private int amountToPay;
    private ArrayList<User> usersWhoUsedService;
    private String reasonForUsing;

    public Service(String nameService, String locationService, int amountToPay,String reasonForUsing) {
        this.nameService = nameService;
        this.locationService = locationService;
        this.amountToPay = amountToPay;
        moneyCollected = 0;
        usersWhoUsedService = new ArrayList<>();
        this.reasonForUsing = reasonForUsing;
    }

    public void payForTheService(User user) throws InterruptedException {
        if (verifyMoneyToPay(user)) {
            user.setUserMoney(user.getUserMoney() - amountToPay);
            moneyCollected += amountToPay;
            user.setReasonForUsing(reasonForUsing);
            usersWhoUsedService.add(user);
        }
    }

    public boolean verifyMoneyToPay(User user) {
        return user.getUserMoney() >= amountToPay;
    }

    public String getNameService() {
        return nameService;
    }

    public String getLocationService() {
        return locationService;
    }

    public int getMoneyCollected() {
        return moneyCollected;
    }

    public ArrayList<User> getUsersWhoUsedService() {
        return usersWhoUsedService;
    }

    public int getAmountToPay() {
        return amountToPay;
    }
}
