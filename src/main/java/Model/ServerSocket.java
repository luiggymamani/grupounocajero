package Model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;

public class ServerSocket implements Runnable{

    int serverPort;
    java.net.ServerSocket serverSocket;
    Socket socket;
    ObjectInputStream receivedPacket;
    UserQueue userQueue;
    Message messages;
    ArrayList<User> users;

    public ServerSocket(int serverPort, UserQueue userQueue) {
        this.serverPort = serverPort;
        this.userQueue = userQueue;
        try {
            serverSocket = new java.net.ServerSocket(serverPort);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void run() {
        System.out.println("\u001B[35m-------------------------");
        System.out.println("\tSERVER STARTED");
        System.out.println("-------------------------\n");
        while (true) {
            try {
                socket = serverSocket.accept();
                receivedPacket = new ObjectInputStream(socket.getInputStream());
                users = (ArrayList<User>) receivedPacket.readObject();
                userQueue.addUsers(users);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
