package Model;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class UserExecutor implements Runnable{
    TellerMachine tellerMachine;
    UserQueue userQueue;
    User currentUser;
    Message messages;

    public UserExecutor(TellerMachine tellerMachine, UserQueue userQueue){
        this.tellerMachine = tellerMachine;
        this.userQueue = userQueue;
        currentUser = null;
        messages = new Message();
        generate100MClients(tellerMachine.getBank());
    }

    @Override
    public void run() {
        while (true) {
            try {
                executeUser();
                TimeUnit.MICROSECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void generate100MClients(Bank bank){
        UserCreator us = new UserCreator();
        ArrayList<User> list = us.generateClient(3000000);
        for (User client: list) {
            bank.registerUser(client);
        }
    }

    public void incrementUsersWaitingShifts(){
        for(int i =0;i<userQueue.size();i++){
            userQueue.getUser(i).incrementWaitingShifts();
        }
    }

    public void executeUser() throws InterruptedException {
        if (!userQueue.isEmpty()) {
            messages.tail(userQueue);
            currentUser = userQueue.deque();
            incrementUsersWaitingShifts();
            currentUser.purpose(tellerMachine);
            messages.actionUser(currentUser.getNameUser());
            messages.userAttended(currentUser, currentUser.getTime());
            messages.finishedUser(currentUser.getNameUser());
            tellerMachine.addUserToATM(currentUser);
            userQueue.sort();
            messages.generalReport(tellerMachine.getLightService().getUsersWhoUsedService().size(),
                                    tellerMachine.getWaterService().getUsersWhoUsedService().size(),
                                    tellerMachine.getInternetService().getUsersWhoUsedService().size(),
                                    tellerMachine.getBank().getRegisteredUsers().size(),
                                    tellerMachine.getBank().getMoneyDeposited());
        }
    }

    public void addUsersToQueue(ArrayList<User> usersList){
        for (User user: usersList) {
            if(user instanceof Client){
                tellerMachine.getBank().registerUser(user);
            }
            userQueue.add(user);
        }
    }


    public User getCurrentUser() {
        return currentUser;
    }
}
