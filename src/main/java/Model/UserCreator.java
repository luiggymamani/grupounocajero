package Model;

import java.util.ArrayList;

public class UserCreator {

    static int userCounter = 0;
    static int userId = 0;
    public boolean getRandomGender(){
        boolean gender;
        int count = (int)(Math.random()*2);
        if(count == 1){
            gender = true;
        }
        else{
            gender = false;
        }
        return gender;
    }

    public int getRandomAge(){
        return (int)(Math.random()*(80-18+1)+25);
    }
    public SpecialCondition generateSpecialCondition (){
        int count = (int)(Math.random()*7);
        switch (count){
            case 1 :
                return SpecialCondition.OTHER;
            case 2 :
                return SpecialCondition.SENIOR_CITIZEN;
            case 3 :
                return  SpecialCondition.PREGNANCY;
            case 4 :
                return SpecialCondition.HANDICAPPED_PERSON;
            case 5 :
                return SpecialCondition.CLIENT;
            case 6 :
                return SpecialCondition.BYTIMEONHOLD;
            default:
                return null;
        }
    }


    public ArrayList<User> generateStandardUser(int quantity){
        ArrayList<User> list = new ArrayList<>();
        for(int i = 0;i<quantity; i++){
            list.add(new StandardUser("userStandard "+userCounter, getRandomAge(), getRandomGender(), 1000, userId));
            userCounter++;
            userId++;
        }
        return list;
    }


    public ArrayList<User> generateSpecialStandardUser(int quantity){
        ArrayList<User> list = new ArrayList<>();
        for(int i = 0;i<quantity; i++){
            StandardUser user = new StandardUser("userSpecial "+userCounter, getRandomAge(), getRandomGender(), 1000, userId);
            user.setIspecial(true, generateSpecialCondition());
            list.add(user);
            userCounter++;
            userId++;
        }
        return list;
    }


    public ArrayList<User> generateClient(int quantity){
        ArrayList<User> list = new ArrayList<>();

        for(int i = 0;i<quantity; i++){
            Client client = new Client("Client "+userCounter, getRandomAge(), getRandomGender(), 1000, userId);
            client.setIspecial(true, SpecialCondition.CLIENT);
            list.add(client);
            userCounter++;
            userId++;
        }
        return list;
    }
}
