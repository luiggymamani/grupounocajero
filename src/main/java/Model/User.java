package Model;

import java.io.Serializable;

abstract public class User implements Serializable {
    private String name;
    private int waitingShifts;
    private int age;
    private boolean isMale;
    private int userMoney;

    private int id;

    private String transactionType;

    private SpecialCondition specialCondition;
    private boolean isSpecial;

    private String reasonForUsing;
    private int time;

    public User(String name, int age, boolean isMale, int UserMoney, int id) {
        this.name = name;
        this.waitingShifts = 0;
        this.age = age;
        this.isMale = isMale;
        this.userMoney = UserMoney;
        this.id = id;
        transactionType =null;
        specialCondition = null;
        isSpecial = false;
        this.reasonForUsing = "";
    }

    public String getNameUser() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWaitingShifts() {
        return waitingShifts;
    }

    public void setWaitingShifts(int waitingShifts) {
        this.waitingShifts = waitingShifts;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getIsMale() {
        return isMale;
    }

    public int getUserMoney() {
        return userMoney;
    }

    public void setUserMoney(int userMoney) {
        this.userMoney = userMoney;
    }


    public SpecialCondition getSpecialCondition() {
        if(isSpecial){
            return specialCondition;
        }
        return null;
    }

    public void setSpecialCondition(SpecialCondition specialCondition) {
        this.specialCondition = specialCondition;
    }


    public boolean isSpecial() {
        return isSpecial;
    }

    public void setIspecial(boolean ispecial, SpecialCondition specialCondition) {
        this.isSpecial = ispecial;
        this.specialCondition = specialCondition;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getReasonForUsing() {
        return reasonForUsing;
    }

    public void setReasonForUsing(String reasonForUsing) {
        this.reasonForUsing = reasonForUsing;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void incrementWaitingShifts(){
        waitingShifts++;
        if(waitingShifts ==10){
            setIspecial(true, SpecialCondition.BYTIMEONHOLD);
        }
    }

    abstract public void  purpose(TellerMachine tellerMachine)throws InterruptedException;
}
