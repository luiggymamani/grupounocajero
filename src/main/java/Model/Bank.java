package Model;

import java.util.HashMap;

public class Bank {

    String nameBank;
    String location;
    int moneyDeposited;
    HashMap<Integer, User> registeredUsers;

    public Bank(String nameBank, String location, int moneyDeposited) {
        this.nameBank = nameBank;
        this.location = location;
        this.moneyDeposited = moneyDeposited;
        registeredUsers = new HashMap<>();
    }

    /**
     * This method allows you to make deposits to the bank.
     *
     * @param money is the money to deposit.
     */
    public void depositMoney(User user, int money) throws InterruptedException {
        if (isRegistered(user) && money <= user.getUserMoney()) {
            moneyDeposited += money;
            user.setUserMoney(user.getUserMoney() - money);
            user.setReasonForUsing("Deposit Money");
        }
    }

    /**
     * This method will allow the user to withdraw money from the bank
     *
     * @param user is the user who will perform the action
     * @param money It is the money that I withdraw from the bank
     */
    public void withdrawMoney(User user, int money) throws InterruptedException {
        if (isRegistered(user) && money <= moneyDeposited) {
            moneyDeposited -= money;
            user.setUserMoney(user.getUserMoney() + money);
            user.setReasonForUsing("Withdraw Money");
        }
    }

    /**
     * This method checks if the user is registered in the bank.
     *
     * @param user is the user to verify
     * @return true or false depending on whether the user is registered.
     */
    public boolean isRegistered(User user) {
        return registeredUsers.get(user.getId()) != null;
    }

    /**
     * This method checks if the user is registered in the bank to perform actions in the bank
     *
     * @param user is the user who will verify
     */
    public void registerUser(User user) {
        if (!isRegistered(user)) {
            registeredUsers.put(user.getId(), user);
        }
    }

    public String getNameBank() {
        return nameBank;
    }

    public String getLocation() {
        return location;
    }

    public int getMoneyDeposited() {
        return moneyDeposited;
    }

    public HashMap<Integer, User> getRegisteredUsers() {
        return registeredUsers;
    }

    public void setMoneyDeposited(int moneyDeposited) {
        this.moneyDeposited = moneyDeposited;
    }
}
