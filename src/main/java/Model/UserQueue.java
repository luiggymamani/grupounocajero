package Model;

import java.util.ArrayList;

public class UserQueue {

    UserNode firstUser;
    UserNode lastUser;

    public UserQueue() {
        firstUser = null;
        lastUser = null;
    }

    public boolean isEmpty() {
        return firstUser == null;
    }

    public int size() {
        int size = 0;
        for (UserNode user = lastUser; user != null; user = user.getNextUser()) size++;
        return size;
    }

    public void add(User user) {
        UserNode newUser = new UserNode(user);
        if (isEmpty()) {
            firstUser = newUser;
            lastUser = newUser;
        } else if (!user.isSpecial()) {
            newUser.setNextUser(lastUser);
            lastUser = newUser;
        } else {
            addSort(newUser);
        }
    }

    /**
     * This method will only add users that are special in order of arrival.
     *
     * @param newUser is the special user to be added.
     */
    private void addSort(UserNode newUser) {
        if (firstUser.getUser().isSpecial()) {
            UserNode firstNonSpecialUser = findTheFirstNonSpecialUser();
            if (firstNonSpecialUser != null) {
                newUser.setNextUser(firstNonSpecialUser.getNextUser());
                firstNonSpecialUser.setNextUser(newUser);
            } else {
                newUser.setNextUser(lastUser);
                lastUser = newUser;
            }
        } else {
            firstUser.setNextUser(newUser);
            firstUser = newUser;
        }
    }

    public void addUsers(ArrayList<User> users) {
        for (User user :  users) add(user);
    }

    /**
     * this method will look for the user that is behind the last user that is special
     * @return the first Non-Special User
     */
    public UserNode findTheFirstNonSpecialUser() {
        if (lastUser.getUser().isSpecial()) return null;
        else {
            for (UserNode user = lastUser; user != null; user = user.getNextUser()) {
                if (user.getNextUser().getUser().isSpecial()) {
                    return user;
                }
            }
        }
        return firstUser;
    }

    /**
     * This method is just to display the user queue.
     * It is used for testing on the console.
     * @return a String to print
     */
    public String showQueue(){
        StringBuilder queue = new StringBuilder();
        for (UserNode user = lastUser; user != null; user = user.getNextUser()){
            queue.append(user.getUser().getNameUser());
            if (user != firstUser) queue.append(", ");
        }
        return queue.toString();
    }

    public User deque() {
        User firstUser = null;
        if (!isEmpty()){
            firstUser = this.firstUser.getUser();
            for (UserNode user = lastUser; user != null; user = user.getNextUser()){
                if (user.getNextUser() == this.firstUser) {
                    user.setNextUser(null);
                    this.firstUser = user;
                } else if (this.firstUser == lastUser) {
                    lastUser = null;
                    this.firstUser = null;
                }
            }
        }
        return firstUser;
    }

    /**
     * This method will order the queue of users if they are special or not.
     */
    public void sort() {
        if (size() > 1) {
            for (int index = 0; index < size() - 1; index++) {
                for (int indexToCompare = index + 1; indexToCompare < size(); indexToCompare++) {
                    if (getUser(index).isSpecial() && !getUser(indexToCompare).isSpecial()) {
                        xchange(index, indexToCompare);
                    }
                }
            }
        }
    }

    public void xchange(int firstIndex, int secondIndex) {
        User auxUser = getUserNode(firstIndex).getUser();
        setNode(firstIndex, getUserNode(secondIndex).getUser());
        setNode(secondIndex, auxUser);
    }

    private void setNode(int index, User user) {
        UserNode newUser = new UserNode(user);
        UserNode currentUser = getUserNode(index);
        UserNode nextUser = currentUser.getNextUser();

        if (index - 1 < 0){
            newUser.setNextUser(nextUser);
            lastUser = newUser;
        } else {
            UserNode previousUser = getUserNode(index - 1);
            previousUser.setNextUser(newUser);
            newUser.setNextUser(nextUser);
            if (currentUser.equals(firstUser)) firstUser = newUser;
        }
    }

    private UserNode getUserNode(int index) {
        UserNode node = lastUser;
        for (int i = 0; i < index; i++) {
            node = node.getNextUser();
        }
        return node;
    }

    public User getUser(int index) {
        return getUserNode(index).getUser();
    }

    public UserNode getFirstUser() {
        return firstUser;
    }

    public UserNode getLastUser() {
        return lastUser;
    }
}
